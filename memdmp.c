//
// memdmp.c
// memdmp
//
// Copyright (c) 2013, Adam Knight
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
// 1. Redistributions of source code must retain the above copyright 
// notice, this list of conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright 
// notice, this list of conditions and the following disclaimer in the 
// documentation and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGE.

#include "memdmp.h"
#include "output.h"

#include <string.h>     // memset, memcmp, memcpy
#include <sys/param.h>  // MIN/MAX
#include <errno.h>      // errno
#include <math.h>       // log10
// #include <stdbool.h>    // bool, true, false


/** Any log, any time, anywhere. */
double nlog(double base, double value);

double nlog(double base, double value)
{
    return (log10(value)/log10(base));
}

/**
 Print a block of memory to stdout in a stylized fashion, ASCII to the right.
 | 00000000 00000000 00000000 00000000 | ................ |
 (Above: four groups with width four)

 @param file File pointer to print to.
 @param data A buffer of data to print.
 @param length The number of bytes from the data pointer to print.
 @param opts An options structure. Pass NULL for defaults.
 @param state Provide a state structure if you will be making several consecutive and related calls (chunks of a file, for instance).  Offset and truncation state will be retained.
*/

ssize_t memdmp(FILE* file, const void* data, size_t length, const struct memdmp_opts* ext_opts, struct memdmp_state* ext_state)
{
    // Defaults
    struct memdmp_opts opts = {
        .base = 16,
        .group = {
            .width = 4,
            .count = 4,
        },
        .display = {
            .color = _useColor(file),
            .address = false,
            .offset = true,
            .encoded = true,
            .text = true,
            .padding = true,
            .truncate = true,
        }
    };
    struct memdmp_state state = {
        .offset = 0,
        .truncated = false,
    };
    
    // Copy these in if they exist.
    if (ext_opts != NULL) opts = *ext_opts;
    if (ext_state != NULL) state = *ext_state;
        
    // Input bytes needed for a full line of output.
    size_t line_width = opts.group.width * opts.group.count;
    
    // Number of output characters needed to represent a byte of input.
    ssize_t byte_size = memstr(NULL, opts.base, data, 1, 0);
    
    off_t offset = 0;
    while (length > offset) {
        // Starting byte position for the current line of input
        const char* line = &((char*)data)[offset];
        
        if ( opts.display.truncate ) {
            if (memcmp(state.prev, line, MIN(line_width, 64)) == 0) {
                // Great! Are we the first one to do this?
                if (!state.truncated) {
                    state.truncated = true;
                    fprintf(file, "%s\n", "...");
                }
                goto NEXT;
            } else if (state.truncated) {
                state.truncated = false;
            }
        }
        
        // Copy line to state.
        memcpy(state.prev, line, line_width);
        state.nprev = line_width;
        
        // Length of this line (considering the last line may not be a full line)
        size_t lineMax = MIN((length - offset), line_width);
        
        // Line header/prefix
        if (opts.display.offset)  fprintf(file, "%#010llx", (unsigned long long)state.offset);
        if ( (opts.display.offset) && (opts.display.address) ) fprintf(file, " @ ");
        if (opts.display.address) fprintf(file, "%16p", (void*)&line[0]);
        if ( (opts.display.offset) || (opts.display.address) ) fprintf(file, ":");
        
        // Print numeric representation
        if (opts.display.encoded) {
            fprintf(file, " ");
            for (size_t c = 0; c < lineMax; c++) {
                if ((c % opts.group.width) == 0) fprintf(file, " ");
                char group[100] = "";
                if (line[c] == 0) {
                    memset(group, '0', byte_size);
                    if (opts.display.color) _print_gray(file, 5, 0);
                } else {
                    memstr(group, opts.base, &line[c], 1, byte_size);
                    if (opts.display.color) _print_color(file, 2, 3, 5, 0);
                }
                
                fprintf(file, "%s%s", group, ((opts.display.padding) ? " " : ""));
                
                if (opts.display.color) _print_reset(file);
            }
        }
        
        // Print ASCII representation
        if (opts.display.text) {
            fprintf(file, " |");
            for (int c = 0; c < lineMax; c++) {
                uint8_t chr = line[c] & 0xFF;
                if (chr == 0)
                    chr = ' ';
                else if (chr > 127) // ASCII unprintable
                    chr = '.';
                else if (chr < 32) // ASCII control characters
                    chr = '.';
                fprintf(file, "%c", chr);
            }
            fprintf(file, "|");
        }
        
        fprintf(file, "\n");
        
    NEXT:
        state.offset += line_width;
        offset += line_width;
    }
    
    // Copy state out.
    if (ext_state != NULL) *ext_state = state;

    return 0;
}



/**
 Get a string representation of a block of memory in any reasonable number base.
 @param out A buffer with enough space to hold the output. Pass NULL to get the size to allocate.
 @param base The base to render the text as (2-36).
 @param data The buffer to render.
 @param nbytes The size of the data in the buffer.
 @param length The size of the output buffer.
 @return The number of characters written to *out (or that would be written), or -1 on error.
 */
ssize_t memstr(char* restrict out, uint8_t base, const void* data, size_t nbytes, size_t length)
{
    // Base 1 would be interesting (ie. infinite). Past base 36 and we need to start picking printable character sets.
    if (base < 2 || base > 36) { errno = EINVAL; return -1; }
    
    // Determine how many characters will be needed in the output for each byte of input (256 bits, 8 bits to a byte).
    uint8_t chars_per_byte = (uint8_t)(ceil(log10(256)/log10(base)));
        
    // Determine the size of the result string.
    size_t rlength = (nbytes * chars_per_byte);
    
    // Return the size if the output buffer is empty.
    if (out == NULL) return rlength;
    
    // Zero length buffer == zero length result.
    if (nbytes == 0) return 0;
    
    // No source buffer?
    if (data == NULL) { errno = EINVAL; return -1; }
    
    // Ensure we have been granted enough space to do this.
    if (rlength > length) { errno = ENOMEM; return -1; }
    
    
    /* Process the string */
    
    // Init the string's bytes to the character zero so that positions we don't write to have something printable.
    // THIS IS INTENTIONALLY NOT '\0'!!
    memset(out, '0', rlength);
    
    // We build the result string from the tail, so here's the index in that string, starting at the end.
    uint8_t ridx = (uint8_t)(rlength - 1);
    for (size_t byte = 0; byte < nbytes; byte++) {
        uint8_t chr = ((uint8_t*)data)[byte];      // Grab the current char from the input.
        while (chr != 0 && ridx >= 0) { // Iterate until we're out of input or output.
            uint8_t idx = chr % base;
            out[ridx] = (idx < 10 ? '0' + idx : 'a' + (idx-10)); // GO ASCII! 0-9 then a-z (up to base 36, see above).
            chr /= base;                // Shave off the processed portion.
            ridx--;                     // Move left in the result string.
        }
    }
    
    // Cap the string.
    out[rlength] = '\0';
    
    return rlength;
}
